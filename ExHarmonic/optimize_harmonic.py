from sampler import *
from state import *
import matplotlib.pyplot as plt

def harmonic(x):
    return 0.5*x**2.


psi=State(-1)
vmc=Sampler()
nsteps=1000
stepsopt=400
learning_rate=0.1

enopt=[]

for i in range(stepsopt):
    vmc.SetPsi(psi)

    elocs=np.zeros(nsteps)
    psider=np.zeros(nsteps)

    for n in range(nsteps):
        vmc.Step(0.1)

        elocs[n]=(vmc.LocalEnergy(harmonic))
        psider[n]=(psi.DerLog(vmc.x))


    energy=np.mean(elocs)
    elocders=(elocs-energy)*psider
    elocder=np.mean(elocders)
    psi.SetParam(psi.GetParam()-learning_rate*elocder)



    enopt.append(energy)
    print "alpha  = ",psi.GetParam()
    print "energy = ",energy," variance = ",np.std(elocs)
    print "gradient variance = ",np.std(elocders),'\n'

plt.plot(enopt)
plt.axhline(0.5,color='black')
plt.ylabel('Energy')
plt.xlabel('#Iteration')
plt.show()
