import numpy as np

class Sampler:
    def __init__(self):
        self.psi=None
        self.x=0
        self.accept=0
        self.moves=0

    def SetPsi(self,Psi):
        self.psi=Psi

    #Markov-Chain transitions
    #the particle is displaced by a uniform random quantity
    #in 0.5[-step_size,step_size]
    def Propose(self,step_size):
        return self.x+(np.random.uniform()-0.5)*step_size

    #Metropolis-Hastings acceptance test
    def Step(self,step_size):
        xprime=self.Propose(step_size)
        #we compute (Psi(xprime)/Psi(x))^2
        #in order to avoid overflow/underflow, we use the logarithm of the wave-function
        if(np.exp(2.*(self.psi.LogVal(xprime)-self.psi.LogVal(self.x))) > np.random.uniform()):
            self.x=xprime
            self.accept+=1
        self.moves+=1


    def Acceptance(self):
        return self.accept/float(self.moves)

    #Local energy (see script)
    def LocalEnergy(self,pot):
        return -0.5*self.psi.Nabla2OPsi(self.x)+pot(self.x)
