#ifndef NQS_HAMILTONIAN_HH
#define NQS_HAMILTONIAN_HH

namespace nqs{
  template<class Graph> class Ising;
}

#include "ising.hh"

#endif
