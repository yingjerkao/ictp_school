#ifndef NQS_GIBBS_SPIN_HH
#define NQS_GIBBS_SPIN_HH

#include <iostream>
#include <Eigen/Dense>
#include <random>
#include <mpi.h>

namespace nqs{

using namespace std;
using namespace Eigen;

//Gibbs sampling for spin variables
template<class RbmState> class Gibbs{

  //number of visible units
  const int nv_;

  //number of hidden units
  const int nh_;

  std::mt19937 rgen_;

  RbmState & rbm_;

  //states of visible and hidden units
  VectorXd v_;
  VectorXd h_;

  //probabilities for gibbs sampling
  VectorXd probv_;
  VectorXd probh_;

  int mynode_;
  int totalnodes_;

public:

  Gibbs(RbmState & rbm):rbm_(rbm),nv_(rbm.Nvisible()),nh_(rbm.Nhidden()){

    v_.resize(nv_);
    h_.resize(nh_);

    probv_.resize(nv_);
    probh_.resize(nh_);

    MPI_Comm_size(MPI_COMM_WORLD, &totalnodes_);
    MPI_Comm_rank(MPI_COMM_WORLD, &mynode_);

    Seed();

    rbm_.RandomVals(v_,rgen_);

    if(mynode_==0){
      cout<<"# Gibbs sampler is ready "<<endl;
    }

  }

  void Seed(int baseseed=0){
    vector<int> seeds(totalnodes_);

    std::random_device rd;

    if(mynode_==0){
      for(int i=0;i<totalnodes_;i++){
        seeds[i]=rd()+baseseed;
      }
    }
    SendToAll(seeds);

    rgen_.seed(seeds[mynode_]);
  }

  void Reset(bool initrandom=false){
    if(initrandom){
      rbm_.RandomVals(v_,rgen_);
    }
  }

  void Sweep(){
    rbm_.ProbHiddenGivenVisible(v_,probh_);
    rbm_.RandomValsWithProb(h_,probh_,rgen_);

    rbm_.ProbVisibleGivenHidden(h_,probv_);
    rbm_.RandomValsWithProb(v_,probv_,rgen_);
  }


  VectorXd Visible(){
    return v_;
  }


  VectorXd Hidden(){
    return h_;
  }

  void SetVisible(const VectorXd & v){
    v_=v;
  }

  void SetHidden(const VectorXd & h){
    h_=h;
  }

  int RandomVisible(){
    std::uniform_int_distribution<int> distribution(0,nv_);
    return distribution(rgen_);
  }

  RbmState & Rbm(){
    return rbm_;
  }

  VectorXd Acceptance()const{
    return VectorXd::Ones(1);
  }

};


}

#endif
