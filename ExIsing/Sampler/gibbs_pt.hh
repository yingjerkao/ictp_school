#ifndef NQS_GIBBS_SPIN_PT_HH
#define NQS_GIBBS_SPIN_PT_HH

#include <iostream>
#include <Eigen/Dense>
#include <random>
#include <mpi.h>

namespace nqs{

using namespace std;
using namespace Eigen;

//Parallel Tempering Gibbs sampling for spin variables
template<class State> class GibbsPt{

  //number of visible units
  const int nv_;

  //number of hidden units
  const int nh_;

  std::mt19937 rgen_;

  State & rbm_;

  //states of visible and hidden units
  std::vector<VectorXd> v_;
  std::vector<VectorXd> h_;

  //probabilities for gibbs sampling
  VectorXd probv_;
  VectorXd probh_;

  int mynode_;
  int totalnodes_;

  const int nrep_;

  vector<double> beta_;

  VectorXd accept_;

  double nsweeps_;

public:

  GibbsPt(State & rbm,int nrep):
    rbm_(rbm),nv_(rbm.Nvisible()),
    nh_(rbm.Nhidden()),
    nrep_(nrep){

    v_.resize(nrep_);
    h_.resize(nrep_);

    accept_.resize(nrep_);

    for(int i=0;i<nrep_;i++){
      v_[i].resize(nv_);
      h_[i].resize(nh_);
    }

    probv_.resize(nv_);
    probh_.resize(nh_);

    MPI_Comm_size(MPI_COMM_WORLD, &totalnodes_);
    MPI_Comm_rank(MPI_COMM_WORLD, &mynode_);

    for(int i=0;i<nrep_;i++){
      beta_.push_back(1.-double(i)/double(nrep_));
    }

    Seed();

    Reset(true);

    if(mynode_==0){
      cout<<"# Parallel Tempered Gibbs sampler is ready "<<endl;
    }

  }

  void Seed(int baseseed=0){
    vector<int> seeds(totalnodes_);

    std::random_device rd;

    if(mynode_==0){
      for(int i=0;i<totalnodes_;i++){
        seeds[i]=rd()+baseseed;
      }
    }
    SendToAll(seeds);

    rgen_.seed(seeds[mynode_]);
  }

  void Reset(bool initrandom=false){

    if(initrandom){
      for(int i=0;i<nrep_;i++){
        rbm_.RandomVals(v_[i],rgen_);
      }
    }

    nsweeps_=0.;
    accept_=VectorXd::Zero(nrep_);
  }

  void Sweep(){

    //First we do serial gibbs steps
    for(int i=0;i<nrep_;i++){
      rbm_.ProbHiddenGivenVisible(v_[i],probh_,beta_[i]);
      rbm_.RandomValsWithProb(h_[i],probh_,rgen_);

      rbm_.ProbVisibleGivenHidden(h_[i],probv_,beta_[i]);
      rbm_.RandomValsWithProb(v_[i],probv_,rgen_);
    }

    //Exchanging
    std::uniform_real_distribution<double> distribution(0,1);

    for(int r=1;r<nrep_;r+=2){
      if(ExchangeProb(r,r-1)>distribution(rgen_)){
        Exchange(r,r-1);
        accept_(r)+=1.;
        accept_(r-1)+=1;
      }
    }

    for(int r=2;r<nrep_;r+=2){
      if(ExchangeProb(r,r-1)>distribution(rgen_)){
        Exchange(r,r-1);
        accept_(r)+=1.;
        accept_(r-1)+=1;
      }
    }

    nsweeps_+=1.;
  }


  VectorXd Visible(){
    return v_[0];
  }


  VectorXd Hidden(){
    return h_[0];
  }


  int RandomVisible(){
    std::uniform_int_distribution<int> distribution(0,nv_);
    return distribution(rgen_);
  }

  //computes the probability to exchange two replicas
  double ExchangeProb(int r1,int r2){
    const double en1=rbm_.Energy(v_[r1],h_[r1]);
    const double en2=rbm_.Energy(v_[r2],h_[r2]);

    return std::exp((beta_[r1]-beta_[r2])*(en1-en2));
  }

  void Exchange(int r1,int r2){
    v_[r1].swap(v_[r2]);
    h_[r1].swap(h_[r2]);
  }

  State & Rbm(){
    return rbm_;
  }

  VectorXd Acceptance()const{
    VectorXd acc=accept_/(2*nsweeps_);
    acc(0)*=2;
    acc(nrep_-1)*=2;
    SumOnNodes(acc);
    acc/=double(totalnodes_);
    return acc;
  }

};


}

#endif
