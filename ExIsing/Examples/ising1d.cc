#include <iostream>
#include "nqs.hh"

using namespace std;
using namespace nqs;

int main(int argc,char * argv[]){
  MPI_Init(&argc,&argv);

  int nspins=20;

  typedef Hypercube Graph;
  Graph lattice(nspins,1);

  typedef Ising<Graph> Hamiltonian;
  Hamiltonian hamiltonian(lattice,1,1);

  int nhidden=20;

  typedef RbmSpin RbmState;
  RbmState rbm(nspins,nhidden);

  rbm.InitRandomPars(1232,0.05);

  typedef Gibbs<RbmState> Sampler;
  Sampler sampler(rbm);

  typedef AdaMax Optimizer;
  Optimizer opt;
  opt.SetResetEvery(1000);

  Variational<Hamiltonian,RbmState,Sampler,Optimizer> var(hamiltonian,sampler,opt);

  int nsamples=1.0e3;
  int niter_opt=10000;

  std::string file_base=to_string(nspins)+"_"+to_string(int(nhidden/nspins));
  var.SetOutName(file_base);

  var.Run(nsamples,niter_opt);

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}
