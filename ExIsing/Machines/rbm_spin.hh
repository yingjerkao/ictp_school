#include <iostream>
#include <Eigen/Dense>
#include <random>
#include <vector>

#ifndef NQS_RBM_SPIN_HH
#define NQS_RBM_SPIN_HH

namespace nqs{

using namespace std;
using namespace Eigen;

//Rbm with s=+/1 values
class RbmSpin{

  //number of visible units
  const int nv_;

  //number of hidden units
  const int nh_;

  //number of parameters
  int npar_;

  //weights
  MatrixXd W_;

  //visible units bias
  VectorXd a_;

  //hidden units bias
  VectorXd b_;

  VectorXd thetas_;
  VectorXd lnthetas_;
  VectorXd thetasnew_;
  VectorXd lnthetasnew_;

  //Useful quantities for safe computation of ln(cosh(x))
  const double log2_;

  const bool usea_;
  const bool useb_;

public:

  RbmSpin(int nv,int nh,bool usea=true,bool useb=true):nv_(nv),
  nh_(nh),W_(nv,nh),
  a_(nv),b_(nh),thetas_(nh),
  lnthetas_(nh),thetasnew_(nh),
  lnthetasnew_(nh),log2_(std::log(2.)),usea_(usea),useb_(useb){

    npar_=nv_*nh_;

    if(usea_){
      npar_+=nv_;
    }
    else{
      a_.setZero();
    }

    if(useb_){
      npar_+=nh_;
    }
    else{
      b_.setZero();
    }

    cout<<"# RBM Initizialized with nvisible = "<<nv_<<" and nhidden = "<<nh_<<endl;

  }

  typedef double StateType;

  int Nvisible()const{
    return nv_;
  }

  int Nhidden()const{
    return nh_;
  }

  int Npar()const{
    return npar_;
  }

  void ProbHiddenGivenVisible(const VectorXd & v,VectorXd & probs,double beta=1){
    logistic(2.*beta*(W_.transpose()*v+b_),probs);
  }

  void ProbVisibleGivenHidden(const VectorXd & h,VectorXd & probs,double beta=1){
    logistic(2.*beta*(W_*h+a_),probs);
  }

  double Energy(const VectorXd & v,const VectorXd & h){
    return -(h.dot(W_.transpose()*v)+v.dot(a_)+h.dot(b_));
  }

  void InitRandomPars(int seed,double sigma){
    auto mask=VectorXd::Ones(npar_);
    InitRandomPars(seed,sigma,mask);
  }

  void InitRandomPars(int seed,double sigma,const VectorXd & mask){
    std::default_random_engine generator(seed);
    std::normal_distribution<double> distribution(0,sigma);

    VectorXd par(npar_);

    for(int i=0;i<npar_;i++){
      par(i)=distribution(generator)*mask(i);
    }

    SetParameters(par);
  }

  VectorXd DerLog(const VectorXd & v){
    VectorXd der(npar_);

    int k=0;

    if(usea_){
      for(;k<nv_;k++){
        der(k)=v(k);
      }
    }

    RbmSpin::tanh(W_.transpose()*v+b_,lnthetas_);

    if(useb_){
      for(int p=0;p<nh_;p++){
        der(k)=lnthetas_(p);
        k++;
      }
    }

    for(int i=0;i<nv_;i++){
      for(int j=0;j<nh_;j++){
        der(k)=lnthetas_(j)*v(i);
        k++;
      }
    }
    return der*0.5;
  }

  VectorXd GetParameters(){

    VectorXd pars(npar_);

    int k=0;

    if(usea_){
      for(;k<nv_;k++){
        pars(k)=a_(k);
      }
    }

    if(useb_){
      for(int p=0;p<nh_;p++){
        pars(k)=b_(p);
        k++;
      }
    }

    for(int i=0;i<nv_;i++){
      for(int j=0;j<nh_;j++){
        pars(k)=W_(i,j);
        k++;
      }
    }

    return pars;
  }

  void SetParameters(const VectorXd & pars){
    int k=0;

    if(usea_){
      for(;k<nv_;k++){
        a_(k)=pars(k);
      }
    }

    if(useb_){
      for(int p=0;p<nh_;p++){
        b_(p)=pars(k);
        k++;
      }
    }

    for(int i=0;i<nv_;i++){
      for(int j=0;j<nh_;j++){
        W_(i,j)=pars(k);
        k++;
      }
    }
  }

  //Value of the logarithm of the wave-function
  double LogVal(const VectorXd & v){
    lncosh(W_.transpose()*v+b_,lnthetas_);

    return (v.dot(a_)+lnthetas_.sum())*0.5;
  }

  //Difference between logarithms of values, when one or more visible variables are being flipped
  VectorXd LogValDiff(const VectorXd & v,const vector<vector<int> >  & toflip,const vector<vector<double>> & newconf){

    VectorXd logvaldiffs;

    const int nconn=toflip.size();
    logvaldiffs.resize(nconn);

    thetas_=(W_.transpose()*v+b_);
    lncosh(thetas_,lnthetas_);

    double logtsum=lnthetas_.sum();

    for(int k=0;k<nconn;k++){
      logvaldiffs(k)=0;

      if(toflip[k].size()!=0){

        thetasnew_=thetas_;

        for(int s=0;s<toflip[k].size();s++){
          const int sf=toflip[k][s];

          logvaldiffs(k)-=2.*a_(sf)*v(sf);

          thetasnew_-=2.*v(sf)*W_.row(sf);
        }

        lncosh(thetasnew_,lnthetasnew_);
        logvaldiffs(k)+=lnthetasnew_.sum() - logtsum;

      }
    }
    return logvaldiffs*0.5;
  }

  //Difference between derivatives of logarithms of values, when one or more visible variables are being flipped
  //Newconf contains the new configuration, but here it is not used since it is redundant information
  MatrixXd DerLogDiff(const VectorXd & v,const vector<vector<int> >  & toflip,const vector<vector<double>> & newconf){

    MatrixXd logvaldiffs;

    const int nconn=toflip.size();
    logvaldiffs=MatrixXd::Zero(nconn,npar_);

    thetas_=(W_.transpose()*v+b_);
    RbmSpin::tanh(thetas_,lnthetas_);

    for(int k=0;k<nconn;k++){

      if(toflip[k].size()!=0){
        thetasnew_=thetas_;

        int p=0;

        auto vprime=v;

        for(int s=0;s<toflip[k].size();s++){

          const int sf=toflip[k][s];
          thetasnew_-=2.*v(sf)*W_.row(sf);

          if(usea_){
            //visible bias part
            logvaldiffs(k,sf)-=2.*v(sf);
            p=nv_;
          }

          vprime(sf)=-v(sf);
        }

        RbmSpin::tanh(thetasnew_,lnthetasnew_);

        if(useb_){
          //hidden bias part
          for(int j=0;j<nh_;j++){
            logvaldiffs(k,p)=lnthetasnew_(j)-lnthetas_(j);
            p++;
          }
        }

        for(int i=0;i<nv_;i++){
          for(int j=0;j<nh_;j++){
            logvaldiffs(k,p)=(lnthetasnew_(j)*vprime(i)-lnthetas_(j)*v(i));
            p++;
          }
        }
      }
    }
    return logvaldiffs*0.5;
  }


  void logistic(const VectorXd & x,VectorXd & y){
    for(int i=0;i<x.size();i++){
      y(i)=logistic(x(i));
    }
  }

  inline double logistic(double x)const{
    return 1./(1.+std::exp(-x));
  }

  void tanh(const VectorXd & x,VectorXd & y){
    for(int i=0;i<x.size();i++){
      y(i)=std::tanh(x(i));
    }
  }

  void lncosh(const VectorXd & x,VectorXd & y){
    for(int i=0;i<x.size();i++){
      y(i)=lncosh(x(i));
    }
  }

  //ln(cos(x)) for real argument
  //for large values of x we use the asymptotic expansion
  inline double lncosh(double x)const{
    const double xp=std::abs(x);
    if(xp<=12.){
      return std::log(std::cosh(xp));
    }
    else{
      return xp-log2_;
    }
  }

  //returns a mask of parameters within a certain distance
  VectorXd ParametersWithinDistance(const vector<vector<int>> & dist,int distcut){
    if(nh_%nv_!=0){
      cerr<<"ParametersWithinDistance should be called only for integer values of Nhidden/Nvisible"<<endl;
      exit(-1);
    }
    assert(dist.size()==nv_);
    assert(distcut>0 || distcut==-1);

    VectorXd mask;
    if(distcut==-1){
      mask.setOnes(npar_);
      return mask;
    }

    mask.setZero(npar_);

    int k=0;
    if(usea_){
      for(;k<(nv_);k++){
        mask(k)=1.0;
      }
    }
    if(useb_){
      for(int p=0;p<(nh_);p++){
        mask(k)=1.0;
        k++;
      }
    }

    for(int i=0;i<nv_;i++){
      for(int j=0;j<nh_;j++){
        int jsite=std::floor(double(j)/double(nv_));
        assert(jsite<nv_);
        if(dist[i][jsite]<distcut){
          mask(k)=1.0;
        }
        k++;
      }
    }
    return mask;
  }

  double RatioVisibleFlip(const VectorXd & v,const VectorXd & h,const vector<int> & toflip){
    double ratio=0;
    for(int k=0;k<toflip.size();k++){
      const int sf=toflip[k];
      assert(sf<nv_ && sf>=0);

      ratio+=h.dot(W_.row(sf))*(-2*v(sf));
      ratio-=2.*v(sf)*a_(sf);
    }
    return std::exp(ratio);
  }

  double RatioHiddenFlip(const VectorXd & v,const VectorXd & h,const vector<int> & toflip){
    double ratio=0;
    for(int k=0;k<toflip.size();k++){
      const int sf=toflip[k];
      assert(sf<nh_ && sf>=0);

      ratio+=v.dot(W_.col(sf))*(-2*h(sf));
      ratio-=2.*h(sf)*b_(sf);
    }
    return std::exp(ratio);
  }

  template<class Rng> void RandomVals(VectorXd & hv,Rng & rgen){
    std::uniform_int_distribution<int> distribution(0,1);

    for(int i=0;i<hv.size();i++){
      hv(i)=2.*distribution(rgen)-1;
    }
  }

  template<class Rng> void RandomValsWithProb(VectorXd & hv,const VectorXd & probs,Rng & rgen){
    std::uniform_real_distribution<double> distribution(0,1);

    for(int i=0;i<hv.size();i++){
      hv(i)=(2.*(distribution(rgen)<probs(i))-1);
    }
  }

};


}

#endif
