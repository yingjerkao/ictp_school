#ifndef NQS_SGD_HH
#define NQS_SGD_HH

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <cassert>
#include <cmath>

namespace nqs{

using namespace std;
using namespace Eigen;

class Sgd{

  //decay constant
  double eta_;

  int npar_;

  double l2reg_;

  double dropout_p_;

  double momentum_;

  std::mt19937 rgen_;

  double decay_factor_;

  VectorXd mask_;

public:

  Sgd(double eta,double l2reg=0,double momentum=0,double dropout_p=0):
    eta_(eta),l2reg_(l2reg),dropout_p_(dropout_p),momentum_(momentum){
    npar_=-1;
    decay_factor_=1;
  }

  void SetNpar(int npar){
    npar_=npar;
    if(mask_.size()!=npar){
      mask_.setOnes(npar_);
    }
  }

  void SetMask(const VectorXd & mask){
    mask_=mask;
  }

  void Update(const VectorXd & grad,VectorXd & pars){
    assert(npar_>0);

    eta_*=decay_factor_;
    double gradnorm=1./sqrt(grad.norm());
    std::uniform_real_distribution<double> distribution(0,1);
    for(int i=0;i<npar_;i++){
      if(distribution(rgen_)>dropout_p_){
        pars(i)=(1.-momentum_)*pars(i) - (grad(i)+l2reg_*pars(i))*eta_*gradnorm*mask_(i);
      }
    }

  }

  void SetDecayFactor(double decay_factor){
    assert(decay_factor<1);
    decay_factor_=decay_factor;
  }

  void Reset(){
  }
};


}

#endif
